# Maintainer: reverseDragon <bergfalkr0@sovietize.github.workersunite>
# this is an experiment to combine PKGBUILDs with Makefiles
pkgname     = wavebuilder
asdname    := $(pkgname)
asdprefix  := wavebuild
pkgdesc     = simple experimental demo of wavebuilder - an increasingly-Hegelian alchemy game
asddesc    := $(pkgdesc)
arch        = any
license     = GPL-3.0-or-later
url         = https://codeberg.org/vxsh-suite/$(pkgname)
# version
major       = 0
minor       = 1
point       = 0
pkgrel      = 4#   insert version into asd file
# dependencies  - Arch package, .asd system
depends     = common-lisp
asddepends  = uiop
source      = $(asdname).asd  $(asdprefix)-combine.lisp  $(asdprefix)-package.lisp
# these will be updated after popping out the pkgbuild
sha256sums  = 1350ab06c81894f351461854d5455fe4581f713b5ad1e0a52b4d1fe6726792f7 2f67b5721c5181c042474c846e4a3c690cafdf633133f5f91a6e10abc03715b0 b203b4a967a1022ff07020560533dd62f86418b1244b95afa346d8e42868fefd
#
# generated PKGBUILD variables
# some of these have to be escaped for both `sh -c` and then make
pkgver      := $(major).$(minor).$(point)
srcdir       = \$${srcdir}
pkgdir       = \$${pkgdir}
_pkgname     = \$${pkgname}
# extra variables
majorver    := $(major).$(minor).
asdver      := $(major).$(minor).$(point).$(pkgrel)
lispsource   = usr/share/common-lisp/source
lispsystems  = usr/share/common-lisp/systems
# author variables
maintainer   = reverseDragon <bergfalkr0@sovietize.github.workersunite>
asdauthor    = R.D. <reversedragon3>


# run "make" and the package will try to build
package: | quickclean $(asdname).asd PKGBUILD
	makepkg PKGBUILD
# run "make install" to try to install a built package
install:
	sudo pacman -U $(pkgname)-*.tar.zst

# link current asd folder into /usr/share for the purposes of working on git repository
develop: $(asdname).asd
	sudo ln -s `pwd` /$(lispsource)/$(asdname)
	sudo ln -s ../source/$(asdname)/$(asdname).asd /$(lispsystems)/$(asdname).asd

# uninstall package and/or development link
uninstall:
	- sudo pacman -R $(pkgname)
	sudo rm -rf /$(lispsource)/$(asdname)
	sudo rm -f  /$(lispsystems)/$(asdname).asd

# generate repository .gitignore
ignore:
	echo -e "$(pkgname)-$(majorver)*\n$(pkgname)_$(majorver)*\npkg\nsrc\nPKGBUILD\n$(asdname).asd\n*.kate-swp" > .gitignore

# run "make clean" to remove build directories & package
asdclean:
	rm -f $(asdname).asd
quickclean: asdclean
	rm -rf pkg src.tar.gz $(pkgname)*.tar.zst PKGBUILD
	rm -rf $(pkgname)_$(majorver)* $(pkgname)-$(majorver)*
srcclean:
	rm -rf src $(pkgname)
clean: quickclean srcclean

.PHONY: asd



null      :=
space     := ${null} ${null}
indent    := ${null} ${null}
bigindent := ${null}             ${null}
smallindent := ${null}   ${null}

# template to produce .asd file
define build-asd
(defsystem \"$(asdname)\" \n\
${indent}:description \"$(asddesc)\" \n\
${indent}:version \"$(asdver)\"   ; major.minor.point.pkgrel \n\
${indent}:author \"$(asdauthor)\" \n\
${indent}:licence \"$(license)\" \n\
${indent}:homepage \"$(url)\" \n\
${indent}\n\
${indent}:depends-on (\"$(subst ${space},"${space}",$(asddepends))\") \n\
${indent}:components ((:file \"$(asdprefix)01-q\")         ; :vxsh-suite.$(asdprefix).q\n\
${indent}${bigindent}(:file \"$(asdprefix)02-hyle\"       ; :vxsh-suite.$(asdprefix).hyle\n\
${indent}${bigindent}${smallindent}:depends-on (\"$(asdprefix)01-q\"))\n\
${indent}${bigindent}(:file \"$(asdprefix)03-combine\"    ; :vxsh-suite.$(asdprefix).combine\n\
${indent}${bigindent}${smallindent}:depends-on (\"$(asdprefix)02-hyle\"))\n\
${indent}${bigindent}(:file \"$(asdprefix)04-dict\")      ; :vxsh-suite.$(asdprefix).dict\n\
${indent}${bigindent}(:file \"$(asdprefix)05-package\"    ; :$(asdprefix)\n\
${indent}${bigindent}${smallindent}:depends-on (\"$(asdprefix)02-hyle\" \"$(asdprefix)03-combine\"))))
endef

$(asdname).asd: asdclean
	echo -e "$(build-asd)" > $(asdname).asd
	chmod 444 $(asdname).asd

asd: asdclean
	echo -e "$(build-asd)" > $(asdname).asd
	chmod 444 $(asdname).asd

# install files into pkg/ tree $pkgdir  - function to be copied to PKGBUILD
# I ran into a huge headache with quotes so I am temporarily using the braille quote ⠴ while I figure it out
define build-package
install -D -m644 ⠴$(srcdir)/$(_pkgname).asd⠴ ⠴$(pkgdir)/$(lispsource)/$(_pkgname)/$(_pkgname).asd⠴ \n\
install -D -m644 ⠴$(srcdir)/$(asdprefix)-combine.lisp⠴ ⠴$(pkgdir)/$(lispsource)/$(_pkgname)/$(asdprefix)-combine.lisp⠴ \n\
install -D -m644 ⠴$(srcdir)/$(asdprefix)-package.lisp⠴ ⠴$(pkgdir)/$(lispsource)/$(_pkgname)/$(asdprefix)-package.lisp⠴ \n\
\n\
mkdir -p ⠴$(pkgdir)/$(lispsystems)⠴ \n\
ln -s ⠴../source/$(_pkgname)/$(_pkgname).asd⠴ ⠴$(pkgdir)/$(lispsystems)/$(_pkgname).asd⠴
endef

# construct PKGBUILD from its component fields prior to running `makepkg`
build-contributors := \# Maintainer: $(maintainer)
build-basics := $(build-contributors)\npkgname=$(pkgname)\npkgver=$(pkgver)\npkgrel=$(pkgrel)\npkgdesc=\"$(pkgdesc)\"\narch=($(arch))\nurl=$(url)\nlicense=('$(license)')
build-files := depends=('$(subst ${space},'${space}',$(depends))')\nsource=('$(subst ${space}${space},'${space}',$(source))')\nsha256sums=('$(subst ${space},'${space}',$(sha256sums))')
build := $(build-basics)\n$(build-files)\n\npackage() {\n $(subst ⠴,\",$(build-package))\n}

PKGBUILD:
	echo -e "$(build)" > PKGBUILD
	updpkgsums  # recalculate sha256sums
