;;;; wavebuild-hyle.lisp - combinable element structure
;; the HYLE structure is defined purely with Lisp's lower-level functions instead of using DEFSTRUCT or CLOS,
;; for the purposes of having simple and self-explanatory code that is easier to maintain or build on
(defpackage :vxsh-suite.wavebuild.hyle
	(:shadowing-import-from :vxsh-suite.wavebuild.q  ; <- this package is used as a namespace
		;; this symbol does nothing so far and is imported as a reminder to have ".q" package accessible
		#:q)
	(:use :common-lisp)
	(:export
		#:element-name-to-symbol #:element-string-to-symbol
		#:hyle  ; used only to mark structures
		#:hyle-p #:make-hyle  #:hyle-fallback-info
		#:hyle-id #:hyle-icon #:hyle-name #:hyle-width #:hyle-moves #:hyle-sum #:hyle-subsetting
		#:hyle< #:hyle-moves-+ #:hyle-sum-+ #:hyle-subset-+
		#:hyle-query
		))
(in-package :vxsh-suite.wavebuild.hyle)


(defun element-name-to-symbol (name)  ; normalize element name to symbol in Q package
	(let (normalized body result)
	(cond
	((symbolp name) name)
	((stringp name)
		(setq
			normalized
				(substitute-if #\-
					(lambda (str)  (find str (list #\  #\.)))
					(string-downcase name))
			body  (string-left-trim "q-" normalized)
			;; remove Q- unless the element is a Wikidata item
			result
				(if (null (parse-integer body :junk-allowed t))
					body  normalized))
		(intern
			(string-upcase (format nil "~a" result))
			:vxsh-suite.wavebuild.q)))))
;; deprecated name
(defun element-string-to-symbol (name)  (element-name-to-symbol name))

(defun hyle-p (element)
	(and
		(vectorp element)
		(equal (elt element 0) 'hyle)))

(defun hyle-id (id)
	(let ((id-symbol
		(cond  ; pull :id value out of HYLE structure
			((hyle-p id)  (elt id 1))
			((stringp id) id)
			(t id))))
	(if (symbolp id-symbol)                 ; if :id is a symbol, and not NIL etc
		(element-name-to-symbol id-symbol)  ; normalize all IDs to same package
		id-symbol)))                        ; otherwise just return it

(defun hyle-icon (element)
	(cond
		((hyle-p element)  (elt element 2))
		((stringp element) element)
		(t element)))

(defun hyle-name (element)
	(cond
		((hyle-p element)  (elt element 3))
		((stringp element) element)
		(t element)))

(defun hyle-width (element)
	(let ((name (hyle-name element)))
	(if (stringp name)
		(length name)
		0)))

(defun hyle-moves (element)
	(cond
		((hyle-p element)  (elt element 4))
		((integerp element) element)
		((stringp element)  1)
		(t nil)))

(defun hyle-sum  (element)
	(cond
		((hyle-p element)   (elt element 5))
		((integerp element) element)
		((stringp element)  1)
		(t nil)))

(defun hyle-subsetting (element)  (elt element 6))

(defun (setf hyle-id)         (value element)  (setf (aref element 1) value))
(defun (setf hyle-icon)       (value element)  (setf (aref element 2) value))
(defun (setf hyle-name)       (value element)  (setf (aref element 3) value))
(defun (setf hyle-moves)      (value element)  (setf (aref element 4) value))
(defun (setf hyle-sum)        (value element)  (setf (aref element 5) value))
(defun (setf hyle-subsetting) (value element)  (setf (aref element 6) value))


(defun hyle-fallback-info (hyle field-func field-override known-hyle)
	(let (result)
	(setf
		result  field-override
		result
			(if (and (null result) (not (null hyle)))
				(funcall field-func hyle)  result)
		result
			(if (and (null result) (not (null known-hyle)))
				(funcall field-func known-hyle)  result
				))))

(defun make-hyle
	(&key id icon name descriptor existing (moves nil movesp) sum subsetting)
	(cond
		;; if passed generic descriptor, apply it as either the name or ID
		((stringp descriptor)
			(setq name descriptor))
		((symbolp descriptor)
			(setq
				id   descriptor
				name
					(if (null descriptor)
						nil
						(symbol-name descriptor)
						)))
		((hyle-p descriptor)
			(setf
				id          (hyle-fallback-info existing   'hyle-id         id    descriptor)
				icon        (hyle-fallback-info descriptor 'hyle-icon       icon  existing)
				name        (hyle-fallback-info descriptor 'hyle-name       name  existing)
				moves       (hyle-fallback-info descriptor 'hyle-moves      moves existing)
				sum         (hyle-fallback-info descriptor 'hyle-sum        sum   existing)
				subsetting  (hyle-fallback-info descriptor 'hyle-subsetting subsetting  existing)
				)))
	;; zero out moves and sum if they are empty in any way
	(setf
		moves
			(if (null moves)
				(if (null movesp)  nil  0)
				moves)
		sum  (if (null sum)  0  sum)
		)
	;; if no :id given, attempt to create one from element name
	(if (null id)  (setq id name))
	;; assuming :id has been created, normalize :id
	(when (or (symbolp id) (stringp id))
		(setq id  (element-name-to-symbol id)))
	;; by default an element is always a subset of itself
	(when (null subsetting)
		(setq subsetting
			(if (null id)  nil  (list id))))
	;; return HYLE structure
	(vector 'hyle id icon name moves sum subsetting))


;;; HYLE "mathematics"

(defun hyle< (a b)  ; return sorting order of two HYLE structures
	(string<
		(cond
			((or (hyle-p a) (symbolp a))
				(symbol-name (hyle-id a)))
			(t ""))
		(cond
			((or (hyle-p b) (symbolp b))
				(symbol-name (hyle-id b)))
			(t ""))))

(defun hyle-moves-+ (a b) ; add up hyle moves
	;; adding up numbers of moves follows the pattern of set theory -
	;; it is as if the plus sign in the equation counts as 1, but otherwise 0 = ∅ = 0
	;; I first used:  (+ a b 1)
	;; but this doesn't get the real number of moves for things like combining the same element, which should only add 1 move despite being weirdly inefficient
	(+ 1  (max (hyle-moves a) (hyle-moves b))))

(defun hyle-sum-+ (a b)  ; add up hyle sums to produce result sum
	;; in general, a :sum cannot be less than 1
	;; some exceptions are granted for the most basic system elements that precede "concept"/HYLE
	(max
		(+ (hyle-sum a) (hyle-sum b))
		1))

(defun hyle-subset-+ (a b)  ; add up total set of ancestors
	;; at first I included 'remove-duplicates until I realized it would be better to leave them for icon inferences
	(append
		(hyle-subsetting a)
		(hyle-subsetting b)))

(defun hyle-query (a b)  ; query used to look up / store element combinations
	(let ((sorted
		(stable-sort (list a b) #'hyle<)))
	(values
		(cons  (hyle-id (first sorted))  (hyle-id (second sorted)))
		sorted)))

;; LATER: when there are inferred combinations, create a function to guess icon based on counting repeated emoji inside element's nearest ancestors
;; icon-strings should be interned as system elements with an :id of their unicode number(s) and a :name of their characters, just to make it really easy to test they are the same
