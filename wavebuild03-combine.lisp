;;;; wavebuild-combine.lisp - combining elements demo
(defpackage :vxsh-suite.wavebuild.combine
	(:shadowing-import-from :vxsh-suite.wavebuild.hyle
		#:element-name-to-symbol
		#:hyle  #:hyle-p #:make-hyle #:hyle-fallback-info
		#:hyle-id #:hyle-icon #:hyle-name #:hyle-width #:hyle-moves #:hyle-sum #:hyle-subsetting
		#:hyle< #:hyle-moves-+ #:hyle-sum-+ #:hyle-subset-+  #:hyle-query)
	(:use :common-lisp)
	(:export
		#:*hyle-width* #:*hyle-table* #:*hyle-list*  ; #::init-hyle-table
		#:hyle-string #:hyle-list
		#:set-hyle #:set-hyle*  #:get-hyle
		#:hyle-mix #:hyle-mix* #:get-combination))
	;; friendly counterparts exported in :wavebuild
		;; #:all #:tall
		;; #:put #:tput  #:net #:tnet
		;; #:mix #:tmix
(in-package :vxsh-suite.wavebuild.combine)


;;; HYLE storage and listing

(defvar *hyle-width*)  ; length of element :name assuming monospace characters
(defvar *hyle-table*)  ; table storing all elements
(defvar *hyle-list*)   ; flat 'list of all elements by internal :id

(defun init-hyle-table ()  ; initialize element metadata
	(setq
		*hyle-table*
			(if (and (boundp '*hyle-table*) (not (null *hyle-table*)))
				*hyle-table*  (make-hash-table :test 'equal))
		*hyle-list*
			(if (and (boundp '*hyle-list*)  (not (null *hyle-list*)))
				*hyle-list*   (list))
		*hyle-width*
			(if (and (boundp '*hyle-width*) (> *hyle-width* 1))
				*hyle-width*   5)
				))

(defun hyle-string (element)  ; string for printing one or more elements to :text
	(init-hyle-table)
	(setq element (make-hyle :descriptor element))
	(let ((max-width (+ *hyle-width* 2))
	      (icon (hyle-icon element))
	      (name (hyle-name element)))
	(setf
		icon  (if (null icon)  "🧩"      icon)
		name  (if (null name)  "[None]"  name)
		)
	(format nil
		;; e.g. "~10A  ~a:~a  ~a"
		(format nil "~~~aA~~a~~%~~~aA  ~~a:~~a  ~~a  ~~a~~%"  3  max-width)
		" "  (remove-duplicates (hyle-subsetting element))
		name  (hyle-sum element) (hyle-moves element)
		(hyle-id element)  icon
		)))

(defun hyle-list (&key text)
;; :wavebuild interface - may be called as 'ALL / 'TALL  -  (all) (tall)
	(init-hyle-table)
	(cond
		((null text) *hyle-list*)
		(t
			(let ((output (make-string-output-stream)))
			(format output "~%")
			(dolist (hyle *hyle-list*)
				(format output "~a~%" (hyle-string (get-hyle hyle))))
			(get-output-stream-string output)
			))))

(defun set-hyle (&key name id icon descriptor moves sum subsetting text)
;; :wavebuild interface - see 'SET-HYLE*
	(init-hyle-table)
	(let (known-hyle hyle icon name-width)
	(setf
		;; attempt to get existing element
		id  (hyle-fallback-info descriptor 'hyle-id id nil)
		known-hyle  (or  (get-hyle id)  (make-hyle))
		;; create element to be stored
		hyle
			(make-hyle
				:descriptor descriptor  :existing known-hyle
				:name name  :id id  :icon icon
				:moves moves  :sum sum  :subsetting subsetting
				)
		name-width  (hyle-width hyle)
		*hyle-width*
			(if (> name-width *hyle-width*)  name-width  *hyle-width*)
		*hyle-list*
			(remove-duplicates (cons id *hyle-list*))
		(gethash id *hyle-table*)  hyle
		)
	(values
		;; return stored HYLE
		(if (null text)  hyle  (hyle-string hyle))
		;; return extra debug information - these should not be taken as a stable API
		*hyle-list*  *hyle-width*)))

(defun set-hyle* (descriptor  &key name id icon moves sum subsetting text)
;; :wavebuild interface - may be called as 'PUT / 'TPUT  -  (put DESCRIPTOR ...) (tput DESCRIPTOR ...)
	(set-hyle :descriptor descriptor
		:name name :id id :icon icon :moves moves :sum sum :subsetting subsetting :text text))

(defun get-hyle (descriptor  &key text)
;; :wavebuild interface - may be called as 'NET / 'TNET  -  (net DESCRIPTOR) (tnet DESCRIPTOR)
	(init-hyle-table)
	(let* ((id   (hyle-id (make-hyle :descriptor descriptor)))
		   (hyle (gethash id *hyle-table*))
		   )
		;; return text if requested, or otherwise return object
		(if (null text)
			hyle  (hyle-string hyle))))

(defun (setf get-hyle) (value element)
	(set-hyle :descriptor element))

(defun get-combination (a b)  ; fetch HYLE structure for combination result
	(multiple-value-bind (query sorted) (hyle-query a b)
	(let* ((lookup
		(gethash query *hyle-table*)))
		;; BUG: don't implement inferred combinations for now
		(values lookup query sorted))))

(defun (setf get-combination) (result hyle-b hyle-a)
	(setf (gethash (hyle-query hyle-a hyle-b) *hyle-table*)  result)
	(set-hyle :descriptor result))


(defun hyle-mix (hyle-a hyle-b  &key result text icon moves sum)  ; combine two elements {{{
;; :wavebuild interface - see 'HYLE-MIX*
	(init-hyle-table)
	(let (hyle1 hyle2 lookup produced result-sum id)

	;; look up combination, always in alphabetical order by internal :id
	(multiple-value-bind (lookup query sorted) (get-combination hyle-a hyle-b)
		(setf
			hyle1   (first  sorted)
			hyle2   (second sorted)
			produced
				(cond
					((not (null result))  (make-hyle :descriptor result))
					((not (null lookup))  lookup)
					(t nil)
					))
		;; if anything was produced, add element and combination to table
		;; remember HYLE structure -  #(HYLE :id :icon :name :moves :sum :subsetting)
		(unless (null produced)
			(setf
				;; use resulting icon unless passed :icon
				(hyle-icon produced)
					(if (null icon)  (hyle-icon produced)  icon)

				moves  (if (null moves)  (hyle-moves result)  moves)  ; use if passed :moves
				sum    (if (null sum)    (hyle-sum result)    sum)    ; use if passed :sum

				(hyle-moves produced)
					(cond
						;; special case: if result appears to be a system element, set :moves to 0
						((and  (not (null result))  (equal (hyle-moves result) 0))
							0)
						((and (integerp moves) (> moves 0))  moves)
						(t (hyle-moves-+ hyle-a hyle-b)))

				(hyle-sum produced)
					(if (and (integerp sum) (> sum 0))
						sum
						(hyle-sum-+ hyle-a hyle-b))

				(hyle-subsetting produced)
					(cons (hyle-id produced) (hyle-subset-+ hyle1 hyle2))

				;; update element in table and store combination
				(get-combination hyle1 hyle2)  produced
				))
		(if (null text)             ; only if terminal-friendly output requested...
			(values produced lookup query result)  ; normally, return debug information
			(hyle-string produced))  ; ...return just an element string
))) ; }}}

(defun hyle-mix* (hyle-a hyle-b  &key result text icon moves sum)  ; fuzzy version of HYLE-MIX {{{
;; :wavebuild interface - may be called as 'MIX / 'TMIX  -  (mix HYLE-A HYLE-B) (tmix HYLE-A HYLE-B RESULT)
	(let (hyle1 hyle2)
	(setq
		;; get element by descriptor
		hyle1  (get-hyle hyle-a)
		hyle2  (get-hyle hyle-b)
		;; fill in elements with null elements if they are missing
		hyle1  (if (null hyle1)  (make-hyle :descriptor hyle-a)  hyle1)
		hyle2  (if (null hyle2)  (make-hyle :descriptor hyle-b)  hyle2)
		)
	(hyle-mix hyle1 hyle2 :result result :text text :icon icon :moves moves :sum sum)
	)) ; }}}
