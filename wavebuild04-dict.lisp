;;;; wavebuild-dict.lisp - parse dictionary files (.dict)
(defpackage :vxsh-suite.wavebuild.dict
	(:shadowing-import-from :vxsh-suite.wavebuild.hyle
		#:element-name-to-symbol
		#:make-hyle  #:hyle-name #:hyle-subsetting
		;; these functions are being referenced semantically as well as for their function code
		#:hyle-id #:hyle-icon #:hyle-moves)
	(:shadowing-import-from :vxsh-suite.wavebuild.combine
		#:hyle-string #:hyle-list
		#:set-hyle
		#:hyle-mix #:hyle-mix*)
	(:use :common-lisp)
	(:export
		#:*line-terminators*  ; #::ensure-line-terminators
		;; #::parse-rule #::parser-at-id #::parser-equal-score #::parser-bracket-icon #::parser-comment-line #::parser-empty-line #::parser-null
		#:line-to-combination #:line-to-list
		#:load-dictionary
		))
(in-package :vxsh-suite.wavebuild.dict)
;; LATER: all tabs may be converted to 4 spaces next update, but NOT this update (2024-03-23)

;; example dictionary line:
;; Q.Hyle.Connector            Q1147639       @Q.Hyle.Dictionary      dictionary          = 0  [📗]

(defvar *line-terminators*)           ; sequence of characters that separate line values
(defun ensure-line-terminators ()     ;   should be a string, with a default of " "
	(if (boundp '*line-terminators*)
		*line-terminators*  " "))

(defun parser-at-id (item)
	(let ((frst (elt item 0)))
	(values
		(if (equal frst #\@)
			(string-left-trim "@ " item)  nil)
		'hyle-id)))

(defun parser-equal-score (item)
	(let ((frst (elt item 0)))
	(values
		(if (equal frst #\=)
			(string-left-trim "= " item)  nil)
		'hyle-moves)))

(defun parser-bracket-icon (item)
	(let ((frst (elt item 0)))
	(values
		(if (equal frst #\[)
			(string-right-trim "] " (string-left-trim "[ " item))
			nil)
		'hyle-icon)))

(defun parser-comment-line (item)
	(let ((frst (elt item 0)))
	(values
		(if (equal frst #\#)
			(string-left-trim "# " item)
			nil)
		'parser-comment-line)))

(defun parser-empty-line (item)
	(let* ((terminators (ensure-line-terminators))
	       (clean (string-right-trim terminators item)))
	(values
		(if (< (length clean) 1)  clean  nil)
		'parser-empty-line)))

(defun parser-null (item)
	(values item nil))

(defun parse-rule (fragment rules)  ; apply parse rule to current string
	(let (result result-rule parsed-item)
	(unless (null fragment)
		(loop  for rule  in rules  until (not (null result))  do
			(multiple-value-bind
				(parse-result parse-func) (funcall rule fragment)
				(setq
					result
						(if (null parse-result)
							result  parse-result)
					result-rule
						(if (null parse-result)
							nil  parse-func)
					))))
	(values result result-rule)))

(defun line-to-list (line)  ; parse text representation of line into a list
	(let (char-value next-char next-char-b current-string parsed-item result-list
	     (total  (- (length line) 1))
	     (current-value  (make-string-output-stream))
	     (current-string-length 0)
	     (terminators  (ensure-line-terminators))
	     (parse-rules  (list 'parser-empty-line 'parser-at-id 'parser-equal-score 'parser-bracket-icon 'parser-null))
	     )
	(loop for i  from 0 to total  do
		(setq
			char-value   (elt line i)
			next-char    (if (< i total)  (elt line (+ i 1))  #\ )
			next-char-b  (if (< (+ i 2) total)  (elt line (+ i 2))  #\ ))

		(unless (and (find char-value terminators) (find next-char terminators))
			(incf current-string-length)
			(format current-value "~a" char-value)
			)
		(cond
			((and (find next-char terminators) (find next-char-b terminators))
				(setq
					current-string
						(string-left-trim terminators
							(get-output-stream-string current-value))
					)
				(multiple-value-bind (parse-result parse-func) (parse-rule current-string parse-rules)
					(setq parsed-item (cons parse-func parse-result))
					)
				(when (> current-string-length 0)
					(setq
						result-list
							(cons
								(if (null parsed-item)
									current-string
									parsed-item)
								result-list)))
				(setq
					;; reset string collectors
					current-string-length  0
					current-value   (make-string-output-stream)
					))))
	(reverse result-list)))

(defun line-to-combination (line-text)
	(let* (item item-rule item-value subset-pieces
	      (value-list     (line-to-list line-text))
	      (total  (- (length value-list) 1))
	      (result-element (make-hyle :moves nil))
	      )
	(loop for i  from 0 to total  do
		(setq
			item  (elt value-list i)
			item-rule  (car item)
			item-value (cdr item)
			subset-pieces
				(if (null item-rule)
					(cons item-value subset-pieces)
					subset-pieces
					))
		;; match list values to HYLE fields
		(cond
			;; assign ID appearing after first two unlabeled values to result element
			((and (equal item-rule 'hyle-id) (>= (length subset-pieces) 2))
				(setf (hyle-id result-element) (element-name-to-symbol item-value)))
			;; assign result element its :moves and :icon
			((equal item-rule 'hyle-moves)
				(setf (hyle-moves result-element) (parse-integer item-value)))
			((equal item-rule 'hyle-icon)
				(setf (hyle-icon result-element)  item-value))
			;; if found, assign third unlabeled value to result element
			((and (null item-rule) (> (length subset-pieces) 2))
				(setf (hyle-name result-element)  item-value))
			;; take the first two unlabeled values and assume result is the combination :subsetting them
			((and (null item-rule) (<= (length subset-pieces) 2))
				(setf (hyle-subsetting result-element)
					(cons
						;; LATER: these are strings. retrieve proper IDs based on synonyms?
						item-value
						(hyle-subsetting result-element)
						)))))
	;; return line information as HYLE structure
	result-element))

(defun load-dictionary (dictionary-path  &key text)
	(let (result-line subset
	     (dictionary-text  ; read dictionary into a list of strings
	       (cond
	         ((pathnamep dictionary-path)
	           (uiop:read-file-lines dictionary-path))
	           (t nil)))
	     (parse-rules (list 'parser-empty-line 'parser-comment-line))
	     )
	;; loop through lines of dictionary parsing each one
	(dolist (line dictionary-text)
		(multiple-value-bind (parse-result parse-func) (parse-rule line parse-rules)
		(cond
			((or (equal parse-func 'parser-comment-line) (equal parse-func 'parser-empty-line))
				'())
			(t
				(setq
					result-line (line-to-combination line)
					subset      (hyle-subsetting result-line))
				;; add combination to model
				(when (>= (length subset) 2)
					(hyle-mix* (first subset) (second subset) :result result-line))
				)))
		)
	(hyle-list :text text)
))
