;;;; wavebuild-q.lisp - namespace for storing elements
(defpackage :vxsh-suite.wavebuild.q
	(:export
		#:q  ; used to mediate package dependency
		;; system elements expected in a typical dictionary
		#:hyle
		#:hyle-id #:hyle-icon #:hyle-name #:hyle-moves #:hyle-sum #:hyle-subsetting
		#:hyle-combination
		#:hyle-dictionary
		))
