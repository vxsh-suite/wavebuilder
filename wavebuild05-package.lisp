;;;; wavebuild-package.lisp - outward-facing interface for beta testing
;; contains short and terminal-friendly versions of inner functions
(defpackage :wavebuild
	(:shadowing-import-from :vxsh-suite.wavebuild.hyle
		#:make-hyle)
	(:shadowing-import-from :vxsh-suite.wavebuild.combine
		#:*hyle-width* #:*hyle-table* #:*hyle-list*
		#:hyle-list #:get-hyle  #:set-hyle #:set-hyle*  #:hyle-mix #:hyle-mix*)
	(:shadowing-import-from :vxsh-suite.wavebuild.dict
		#:load-dictionary)
	(:use :common-lisp)
	(:export
		#:make-hyle
		#:*hyle-table* #:*hyle-list*
		#:hyle-list #:get-hyle  #:set-hyle #:set-hyle*  #:hyle-mix #:hyle-mix*
		#:load-dictionary
		#:net #:all  #:put #:mix  #:tnet #:tput #:tall #:tmix))
(in-package :wavebuild)


;; basic commands that return objects

;; you can't call a function GET or SET while using :common-lisp
(defun net (descriptor)  (get-hyle descriptor))

(defun all ()  (hyle-list))

(defun put (descriptor  &key name id icon moves sum subsetting)
	(set-hyle :descriptor descriptor
		:name name :id id :icon icon :moves moves :sum sum :subsetting subsetting))

(defun mix (hyle1 hyle2  &optional result)
	(hyle-mix* hyle1 hyle2  :result result))


;; terminal-friendly commands that return text - "t" either stands for "text" or "terminal"

(defun tnet (descriptor)  (get-hyle descriptor :text t))

(defun tput (descriptor  &key name id icon moves sum subsetting)
	(set-hyle :descriptor descriptor
		:name name :id id :icon icon :moves moves :sum sum :subsetting subsetting :text t))

(defun tall ()  (hyle-list :text t))

(defun tmix (hyle1 hyle2  &optional result)
	(hyle-mix* hyle1 hyle2  :result result  :text t))
